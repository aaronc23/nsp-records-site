import {
	getChapter,
	getChapters,
} from './ChaptersController';

import {
	withDBConnection,
	isLoggedIn,
} from '../util';

export default app => {

	//Returns a list of all chapters
	app.route('/api/chapters').get(isLoggedIn, withDBConnection(getChapters));
	
	//Returns a specific chapter
	app.route('/api/chapters/:id').get(isLoggedIn, withDBConnection(getChapter));

}

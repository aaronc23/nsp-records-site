import * as STATUS_CODES from 'http-status-codes';
import { buildSQL } from './SQLBuilder';
import { returnSQLResponse } from '../util';

const transformResults = results => {
	if (results.length < 1) return results;
	if (results[0].name) {
		const rObj = {};
		results.forEach(element => {
			rObj[element.name] = element.result;
		});
		return rObj;
	}
	return { result: results[0].result }
}

export const Analyize = (request, response, connection) => {
	const { field, entity, query } = request.query;
	const { method } = request.params;
	
	let sql;
	if ((field || entity) && query) {
		response.status(STATUS_CODES.BAD_REQUEST).json({result: 0, reason: 'query cannot be defined if field or entity is defined'});
	}

	if (field && !entity) {
		response.status(STATUS_CODES.BAD_REQUEST).json({result: 0, reason: 'entity must be supplied if field is supplied'});
	}
	if (entity || field) {
		sql = buildSQL(method, `${entity}${field ? ` (${field})` : ''} `);
	} else {
		sql = buildSQL(method, query);
	}

	if (typeof sql !== 'string') {
		response.status(STATUS_CODES.BAD_REQUEST).json({result: 0, sql});
	} else {
		connection.query(sql, returnSQLResponse(connection, response, transformResults));
	}
}
import sendEmail from './email-client';

export const requestContactInfo = (request, response, connection) => {

	const recipient = request.user.email;
	let { rallyID } = request.params;

	connection.query(`
		SELECT *,schools.name AS 'school_name' 
		FROM
			rallies 
			JOIN 
			school_registration
				ON rallies.schoolreg_id=school_registration.registration_id
			JOIN schools
				ON school_registration.school_id = schools.id
		WHERE rallies.id=${rallyID}`,

		(error, results) => {
			connection.release();
			let template = '';
			let parameters = {};

			if (error) {
				parameters = {
					error: error.message,
					rallyID,
					recipient,
				};

				template = '/contact-info-request-failure.html';
			} else {
				parameters = results[0];
				parameters.recipient = recipient;

				template = '/contact-info-request.html';
			}

			sendEmail(
				'joseph.s@nationalschoolproject.com',
				'Speaker/Performer Contact Information Request',
				template,
				parameters,
				response
			);

		})

}

export const suggestEdit = (request, response) => {

	const recipient = request.user.email;
	let { type, id, edit } = request.params

	sendEmail(
		'joseph.s@nationalschoolproject.com',
		{
			template: 'Event Edit Request ({type})',
			parameters: { type }
		},
		'/event-edit-request.html',
		{
			recipient,
			type,
			id,
			edit
		},
		response
	)
}
